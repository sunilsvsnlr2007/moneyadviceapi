﻿using CCRM.AC.SpecialOffer.Model;
using InsuranceDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace CCRM.AC.SpecialOffer.BusinessLayer.Query
{
    public class GenerateQuery
    {
        public SpecialCodesResponse GetQualifiedCodes(InputFields fields)
        {
            try
            {
                App.Logger.Info(ResxMessages.Msg_GetQualifiedCodesInitiated);
                
                IQueryable<SpecialCodes> lstSplCodes = App.insuranceDBContext.SpecialOfferRecord.AsQueryable();

                ///Query Part One contains as follows
                ///Check Status
                ///SimCan, Indexation, Conversion
                ///Between Valid From and To Date
                ///Checking Type
                lstSplCodes = PreparingQueryOne(fields, lstSplCodes);

                ///Query Part Two contains as follows
                ///Between Min and Max Age
                ///Between Min and Max Annual Premium
                ///Between Min and Max Sum Assured
                lstSplCodes = PreparingQueryTwo(fields, lstSplCodes);

                ///Query Part Three contains as follows
                ///Products and its related Benefits
                ///Matches
                ///Applies To
                lstSplCodes = PreparingQueryThree(fields, lstSplCodes);

                ///Query Part Four contains as follows
                ///Commission
                ///Deferred
                ///Life Basis
                ///Pay Frequency
                lstSplCodes = PreparingQueryFour(fields, lstSplCodes);

                //Executing Query
                List<SpecialCodes> splCodesCollection = lstSplCodes.AsQueryable().ToList();

                //Preparing Response Object
                SpecialCodesResponse response = PrepareResponseObject(fields, splCodesCollection);

                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private IQueryable<SpecialCodes> PreparingQueryOne(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            //Query for Status, SimCan, Indexation, Conversion
            if (!string.IsNullOrEmpty(fields.SimCan) || !string.IsNullOrEmpty(fields.Indexation) || !string.IsNullOrEmpty(fields.Conversion))
            {
                lstSplCodes = PrepareSCIC(fields, lstSplCodes);
            }

            //Query to filter Valid From and To Date
            lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, v => v.ValidFrom <= DateTime.UtcNow && v.ValidTo >= DateTime.UtcNow && v.Deleted.Equals(0));

            //If it is Special Offer - We need to send as S, for Vocher - V
            if (!string.IsNullOrEmpty(fields.filterType))
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, f => f.TypeDescription.Equals(fields.filterType, StringComparison.InvariantCultureIgnoreCase) && f.Deleted.Equals(0));
            }
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PreparingQueryTwo(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            //Query to find Between Min and Max Age
            if (fields.Age > 0)
                lstSplCodes = PrepareAgeMinMaxQuery(fields, lstSplCodes);

            //Query to find Between Annual Preminum
            if (fields.AnnualPremium > 0)
                lstSplCodes = PrepareAnnualPremiumQuery(fields, lstSplCodes);

            //Query to find Between Sum Assured
            if (fields.SumAssured > 0)
                lstSplCodes = PrepareSumAssuredQuery(fields, lstSplCodes);
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PreparingQueryFour(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            //Query for Commission
            if (fields.SelectedComission != null && fields.SelectedComission.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstComissions.Any(comm => fields.SelectedComission.Contains(comm.ComissionName) && comm.Deleted.Equals(0)));
            }

            //Query for Deferred
            if (fields.SelectedDeferred != null && fields.SelectedDeferred.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstDeferred.Any(d => fields.SelectedDeferred.Contains(d.DeferredName) && d.Deleted.Equals(0)));
            }

            //Query for Life Basis
            if (fields.SelectedLifeBasis != null && fields.SelectedLifeBasis.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstLifeBasis.Any(l => fields.SelectedLifeBasis.Contains(l.LifeBasisName) && l.Deleted.Equals(0)));
            }

            //Query for Pay Frequency
            if (fields.SelectedPayFreq != null && fields.SelectedPayFreq.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstPayFrequency.Any(p => fields.SelectedPayFreq.Contains(p.PayFrequencyName) && p.Deleted.Equals(0)));
            }
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PreparingQueryThree(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            //Query for products and its related benefits
            if (fields.SelectedProducts != null && fields.SelectedProducts.Count > 0)
            {
                lstSplCodes = PrepareProductsQuery(fields, lstSplCodes);
            }

            //Query for Matches
            if (fields.SelectedMatches != null && fields.SelectedMatches.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstMatches.Any(m => fields.SelectedMatches.Contains(m.MatchName) && m.Deleted.Equals(0)));
            }

            //Query for Applies To
            if (fields.SelectedAppliesTo != null && fields.SelectedAppliesTo.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstAppliesTo.Any(a => fields.SelectedAppliesTo.Contains(a.AgencyCode) && a.Deleted.Equals(0)));
            }

            //Query for Exclusions To
            if (fields.SelectedExclusions != null && fields.SelectedExclusions.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstExclusions.Any(a => !fields.SelectedExclusions.Contains(a.AgencyCode) && a.Deleted.Equals(0)));
            }
            return lstSplCodes;
        }

        private SpecialCodesResponse PrepareResponseObject(InputFields fields, List<SpecialCodes> splCodesCollection)
        {
            App.Logger.Info(ResxMessages.Msg_QualifiedRecordsCount + splCodesCollection.Count);
            App.Logger.Info(ResxMessages.Msg_StartPrintingSpecialCodes);
            SpecialCodesResponse splCodesResponse = new SpecialCodesResponse();

            splCodesCollection.ForEach(c =>
            {
                App.Logger.Info(ResxMessages.Msg_QualifiedCode + c.Id);
                SpecialOfferCode splCode = new SpecialOfferCode();
                splCode.Code = c.Id;
                splCode.OnlineVocherDescription = c.OnlineOrVoccherDescription;

                c.LstTranches.Where(d => d.LstMatches.Any(e => fields.SelectedMatches.Contains(e.MatchName) && e.Deleted.Equals(0))).ToList().ForEach(d =>
                {
                    splCode.LstTrancheRecords.Add(new TranscheRecords() { InternalDescription = d.InternalDescription, Percentage = d.Percentage });
                });

                splCodesResponse.LstSplOfferCodes.Add(splCode);
            });

            App.Logger.Info(ResxMessages.Msg_EndedPrintingSpecialCodes);
            return splCodesResponse;
        }

        private IQueryable<SpecialCodes> PrepareSCIC(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.StatusId.Equals((int)StatusEnum.Active));

            if (!string.IsNullOrEmpty(fields.SimCan) && !fields.SimCan.Equals(YesNoEnum.YN.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, s => s.SimCan.Equals(fields.SimCan, StringComparison.InvariantCultureIgnoreCase) && s.Deleted.Equals(0));
            }

            if (!string.IsNullOrEmpty(fields.Indexation) && !fields.Indexation.Equals(YesNoEnum.YN.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, s => s.Indexation.Equals(fields.Indexation, StringComparison.InvariantCultureIgnoreCase) && s.Deleted.Equals(0));
            }

            if (!string.IsNullOrEmpty(fields.Conversion) && !fields.Conversion.Equals(YesNoEnum.YN.ToString(), StringComparison.InvariantCultureIgnoreCase))
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, s => s.Conversion.Equals(fields.Conversion, StringComparison.InvariantCultureIgnoreCase) && s.Deleted.Equals(0));
            }
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PrepareSumAssuredQuery(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, s => s.SumAssuredMin <= fields.SumAssured && (s.SumAssuredMax >= fields.SumAssured || s.SumAssuredMax.Equals(0)) && s.Deleted.Equals(0));
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PrepareAnnualPremiumQuery(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, a => a.APMin <= fields.AnnualPremium && (a.APMax >= fields.AnnualPremium || a.APMax.Equals(0)) && a.Deleted.Equals(0));
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PrepareAgeMinMaxQuery(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, a => a.AgeMin <= fields.Age && (a.AgeMax >= fields.Age || a.AgeMax.Equals(0)) && a.Deleted.Equals(0));
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> PrepareProductsQuery(InputFields fields, IQueryable<SpecialCodes> lstSplCodes)
        {
            if (fields.SelectedBenefits != null && fields.SelectedBenefits.Count > 0)
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstProducts.Any(p => fields.SelectedProducts.Contains(p.ProductName) && p.Deleted.Equals(0) && p.LstBenefits.Any(b => fields.SelectedBenefits.Contains(b.BenefitName) && b.Deleted.Equals(0))));
            }
            else
            {
                lstSplCodes = ApplySpecialCodesFilter(lstSplCodes, c => c.LstProducts.Any(p => fields.SelectedProducts.Contains(p.ProductName) && p.Deleted.Equals(0)));
            }
            return lstSplCodes;
        }

        private IQueryable<SpecialCodes> ApplySpecialCodesFilter(IQueryable<SpecialCodes> splCodes,
                                                                          Expression<Func<SpecialCodes, bool>> predicate)
        {
            return splCodes.Where(predicate);
        }
    }
}
