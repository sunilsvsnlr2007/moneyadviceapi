﻿using InsuranceDB.DBFolder;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.BusinessLayer
{
    public class App
    {
        public static InsuranceDBContext insuranceDBContext = new InsuranceDBContext();
        public static Logger Logger = LogManager.GetLogger("MoneyAdviceAPI");
    }
}
