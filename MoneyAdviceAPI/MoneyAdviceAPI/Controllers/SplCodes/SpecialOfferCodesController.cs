﻿using CCRM.AC.SpecialOffer.BusinessLayer;
using CCRM.AC.SpecialOffer.BusinessLayer.Query;
using CCRM.AC.SpecialOffer.Model;
using MoneyAdvice.CRM.Infrastructure.WebApiHandler;
using MoneyAdvice.CRM.Infrastructure.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MoneyAdvice.CRM.Infrastructure.ExceptionHandler;
using MoneyAdvice.CRM.Infrastructure.Messages.ResxCommon;
using Caledonian.CRM.DapperRepository.Contracts;
using Caledonian.CRM.DapperRepository.Impl;
using Caledonian.CRM.DataEntity.Model.Dtos;

namespace CCRM.AC.SpecialOffer.WebApi.Controllers.SplCodes
{
    public class SpecialOfferCodesController : ApiController
    {
        UserIdentityStruct identityStruct;
        [ActionName("FetchCodes")]
        [HttpPost]
        public ServiceResponse<SpecialCodesResponse> FetchCodes(ServiceRequest<InputFields> ipFields)
        {
            int totalRecordsCount = 0;
            SpecialCodesResponse response = new SpecialCodesResponse();
            CredentialResponse resp = new CredentialResponse();
            try
            {
                if (ipFields.Data == null)
                {
                    throw new MoneyAdviceCRMDataValidationException(ResxError.ERR_00003_InvalidRequest);
                }

                using (IDapperUoW vow = DapperRepositoryManager.GetUnitOfWork(identityStruct))
                {
                    using (IDapperCrmRepository rep = DapperRepositoryManager.GetCrmRepository(vow))
                    {
                        resp = rep.ValidateCredentials(ipFields.Data.Header.UserName, ipFields.Data.Header.Password, ipFields.Data.Header.Pin, out identityStruct);
                    }
                }
                
                GenerateQuery query = new GenerateQuery();
                response = query.GetQualifiedCodes(ipFields.Data);
                response.input = ipFields.Data;

                if (response.LstSplOfferCodes != null && response.LstSplOfferCodes.Count == 0)
                {
                    throw new MoneyAdviceCRMDataNotFoundException(ResxError.ERR_00013_NoDataFound);
                }

                totalRecordsCount = response.LstSplOfferCodes.Count;

                return WebApiRequestValidator.HandleResponse<InputFields, SpecialCodesResponse>(AppSourceEnum.TeamCrm, null, ipFields, response, totalRecordsCount, resp.UserId, false, false);
            }
            catch (Exception ex)
            {
                App.Logger.ErrorException(ex.Message, ex);
                return WebApiRequestValidator.HandleResponse<InputFields, SpecialCodesResponse>(AppSourceEnum.TeamCrm, ex, ipFields, response, totalRecordsCount, resp.UserId, false, false);
            }
        }
    }
}
