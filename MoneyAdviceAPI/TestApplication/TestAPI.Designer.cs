﻿namespace TestApplication
{
    partial class TestAPI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSplCodes = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSplCodes
            // 
            this.btnSplCodes.Location = new System.Drawing.Point(176, 41);
            this.btnSplCodes.Name = "btnSplCodes";
            this.btnSplCodes.Size = new System.Drawing.Size(96, 23);
            this.btnSplCodes.TabIndex = 0;
            this.btnSplCodes.Text = "Get Spl Codes";
            this.btnSplCodes.UseVisualStyleBackColor = true;
            this.btnSplCodes.Click += new System.EventHandler(this.btnSplCodes_Click);
            // 
            // TestAPI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnSplCodes);
            this.Name = "TestAPI";
            this.Text = "TestAPI";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSplCodes;
    }
}