﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public class Errors
    {
        public Errors()
        {
            ErrorMessage = string.Empty;
        }
        public string ErrorMessage { get; set; }
    }
}
