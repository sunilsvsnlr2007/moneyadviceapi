﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public class InputFields
    {
        public InputFields()
        {
            filterType = string.Empty;
            Indexation = string.Empty;
            Conversion = string.Empty;
            SimCan = string.Empty;
            Age = 0;
            AnnualPremium = 0;
            SumAssured = 0;
            SelectedBenefits = new List<string>();
            SelectedProducts = new List<string>();
            SelectedDeferred = new List<string>();
            SelectedComission = new List<string>();
            SelectedLifeBasis = new List<string>();
            SelectedPayFreq = new List<string>();
            SelectedMatches = new List<string>();
            SelectedAppliesTo = new List<string>();
            Header = new Header();
        }

        /// <summary>
        /// If we pass S - it verfies for Special Offers
        /// If we pass V - it verfies for Vochers
        /// </summary>
        public string filterType { get; set; }

        /// <summary>
        /// Y - Fetch records when indexation 'Y'
        /// N - Fetch records when indexation 'N'
        /// YN - Fetch records when indexation 'YN'
        /// '' - It wont check for indexation
        /// </summary>
        public string Indexation { get; set; }

        /// <summary>
        /// Y - Fetch records when conversion 'Y'
        /// N - Fetch records when conversion 'N'
        /// YN - Fetch records when conversion 'YN'
        /// '' - It wont check for conversion
        /// </summary>
        public string Conversion { get; set; }

        /// <summary>
        /// Y - Fetch records when SimCan 'Y'
        /// N - Fetch records when SimCan 'N'
        /// YN - Fetch records when SimCan 'YN'
        /// '' - It wont check for SimCan
        /// </summary>
        public string SimCan { get; set; }

        /// <summary>
        /// >It pulls the records in between this value
        /// </summary>
        public decimal AnnualPremium { get; set; }

        public int Age { get; set; }
        public decimal SumAssured { get; set; }

        /// <summary>
        /// it holds the Name column data and query based on its values
        /// </summary>
        public List<string> SelectedProducts { get; set; }
        public List<string> SelectedBenefits { get; set; }
        public List<string> SelectedAppliesTo { get; set; }
        public List<string> SelectedMatches { get; set; }
        public List<string> SelectedDeferred { get; set; }
        public List<string> SelectedComission { get; set; }
        public List<string> SelectedLifeBasis { get; set; }
        public List<string> SelectedPayFreq { get; set; }
        public List<string> SelectedExclusions { get; set; }

        public Header Header { get; set; }
    }
}
