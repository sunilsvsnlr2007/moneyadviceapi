﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public class SpecialCodesResponse
    {
        public SpecialCodesResponse()
        {   
            LstErrors = new List<Errors>();
            input = new InputFields();
            LstSplOfferCodes = new List<SpecialOfferCode>();
        }

        //In future we may get chance to show multiple errors.
        public List<Errors> LstErrors { get; set; }

        public InputFields input { get; set; }

        public List<SpecialOfferCode> LstSplOfferCodes { get; set; }
    }
}
