﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public class SpecialOfferCode
    {
        public SpecialOfferCode()
        {
            Code = 0;
            OnlineVocherDescription = string.Empty;
            LstTrancheRecords = new List<TranscheRecords>();
        }
        public int Code { get; set; }
        public string OnlineVocherDescription { get; set; }
        public List<TranscheRecords> LstTrancheRecords { get; set; }
    }
}
