﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public class Header
    {
        public Header()
        {
            UserName = string.Empty;
            Password = string.Empty;
        }
        public string UserName { get; set; }
        public string Password { get; set; }
        public IDictionary<int,string> Pin { get; set; }
    }
}
