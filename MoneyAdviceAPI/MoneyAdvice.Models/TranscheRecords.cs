﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public class TranscheRecords
    {
        public TranscheRecords()
        {
            InternalDescription = string.Empty;
            Percentage = 0;
        }
        public string InternalDescription { get; set; }
        public decimal Percentage { get; set; }
    }
}
