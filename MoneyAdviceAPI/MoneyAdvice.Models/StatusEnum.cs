﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CCRM.AC.SpecialOffer.Model
{
    public enum StatusEnum
    {
        Active = 64000,
        Closed = 64001
    }
}
